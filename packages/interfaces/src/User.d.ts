export interface User {
  userID: string

  appleID?: string
  facebookID?: string
  googleID?: string
}

export interface UserAccount {
  userID: string

  name: string
  email: string
  zip: number
  birthdate: Date
  password: string
}

export interface UserBiometrics {
  userID: string

  bodyMassIndex?: number
  gender?: 'male' | 'female'
  heightFeet?: number
  heightInches?: number
  mobility: 'low' | 'medium' | 'high'
  surgery?: '0-6' | '6-12' | '12-18' | '18-36'
  weight?: number
}

export interface UserSubscription {
  userID: string

  startDate: Date
  stripeToken: string
}
