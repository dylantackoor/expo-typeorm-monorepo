import { Ingredient } from './Diet'

export interface Plan {
  start: Date
  end: Date
  shoppingList: Ingredient[]
}
