# Example

## Quick Start

Install packages

```sh
npm i -g yarn
yarn install
```

Run Expo ([README](./packages/app/README.md))

```sh
cd packages/app
yarn run develop
```

Run Express ([README](./packages/server/README.md))

```sh
cd packages/server
yarn run develop
```

## Workspace Wide Commands

These are run against staged files, but you may want to validate changes earlier.

```sh
yarn run markdownlint # lints .md files againt [config](https://google.com)
yarn run format # prettier formats all js,ts,jsx,tsx,json,yml,yaml
yarn run lint # eslint against all js,ts,jsx,tsx
yarn run stylelint # Validates styles for css,jsx,tsx
yarn run types # checks all typings without building
```

## TODOs

- [x] workspace package import example
- [x] commit hooks
- [x] markdown linting + config
- [x] app wide formatting
- [x] per package stylelint configs + shared base + workspace wide script
- [x] per package eslint configs + shared base + workspace wide script
- [x] per package tsconfig + shared base + workspace wide script
- [ ] import existing Expo app
- [ ] configure Express launch.json
