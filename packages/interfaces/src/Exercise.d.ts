import { Post } from './shared'

export interface Exercise extends Post {
  type: 'Exercise'
  repitions: number
  sets: number
}
