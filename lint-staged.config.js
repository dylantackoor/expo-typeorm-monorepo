module.exports = {
  '*.{js,jsx,ts,tsx,d.ts,json,md,yml,yaml}': 'prettier --write',
  // '*.{js,jsx,ts,tsx,d.ts}': 'eslint --fix --cache --cache-location .eslintcache',
  // '*.{js,jsx,ts,tsx}': 'stylelint --cache --cache-location .stylelintcache',
  '*.md': 'markdownlint --fix',
}
