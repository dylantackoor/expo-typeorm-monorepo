export interface Post {
  postID: string

  title: string
  description: string

  videoURL?: string
  blogContent?: string
}
