import { Post } from './shared'

export interface Lesson extends Post {
  type: 'Education'
  category: 'Fitness' | 'Nutrition' | 'Motivational' | 'Surgery'
}
