import { Router } from 'express'

import { UserAccount } from '@dylantackoor/types'

export function indexRouter(): Router {
  return Router().get('/', async (_req, res) => {
    const data: UserAccount = {
      name: 'Dylan',
      age: 25,
    }

    res.json(data)
  })
}
