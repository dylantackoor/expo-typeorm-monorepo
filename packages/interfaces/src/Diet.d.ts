export interface Ingredient {
  ingredientID: string
  name: string
  quantity: string
}

export interface Recipe {
  recipeID: string

  name: string

  calories: number
  fat: number
  protein: number

  ingredients: Ingredient[]
  directions: string
}
