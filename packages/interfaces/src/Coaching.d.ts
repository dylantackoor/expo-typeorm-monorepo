export interface Message {
  fromID: string
  toID: string
  timestamp: Date
  content: string
  read: boolean
}
