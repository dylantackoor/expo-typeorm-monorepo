const isStrictMode = process.env.CI === 'true'

module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  env: {
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier',
    'prettier/@typescript-eslint',
  ],
  rules: {
    'linebreak-style': ['error', 'unix'],
    '@typescript-eslint/no-unused-vars': isStrictMode ? 'error' : 'warn',
    'no-unneeded-ternary': 'error',
    'no-warning-comments': isStrictMode
      ? ['error', { terms: ['fixme'], location: 'anywhere' }]
      : 'warn',
  },
}
